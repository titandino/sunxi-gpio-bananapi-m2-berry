#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "sunxi_dht_read.h"

#define GPIO_LINE_NUMBER 35
#define PORT 5000
#define BUFFER_SIZE 2048

void setup_socket(int *server_fd, struct sockaddr_in *address) {
    int opt = 1;
    *server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (*server_fd == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(*server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }

    address->sin_family = AF_INET;
    address->sin_addr.s_addr = INADDR_ANY;
    address->sin_port = htons(PORT);

    if (bind(*server_fd, (struct sockaddr *)address, sizeof(*address)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(*server_fd, 3) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }
}

void respond_with_sensor_data(int client_sock) {
    float humidity = 0, temperature = 0;
    float cachedHumidity = -1, cachedTemperature = -1;
    char buffer[BUFFER_SIZE] = {0};

    int result = sunxi_dht_read(DHT22, GPIO_LINE_NUMBER, &humidity, &temperature);
    if (result == DHT_SUCCESS) {
        cachedHumidity = humidity;
        cachedTemperature = (temperature * 9.0 / 5.0) + 32;
    }

    char response[512];
    int contentLength;

    if (cachedHumidity != -1 && cachedTemperature != -1) {
        contentLength = snprintf(response, sizeof(response), "{\"humidityPerc\": \"%.2f\", \"tempFahrenheit\": \"%.2f\"}", cachedHumidity, cachedTemperature);
        snprintf(buffer, BUFFER_SIZE, 
                 "HTTP/1.1 200 OK\r\n"
                 "Content-Type: application/json\r\n"
                 "Content-Length: %d\r\n"
                 "Access-Control-Allow-Origin: *\r\n" // Allow any origin
                 "Connection: close\r\n\r\n%s", 
                 contentLength, response);
    } else {
        contentLength = snprintf(response, sizeof(response), "{\"error\": \"Failed to read from sensor, error code: %d\"}", result);
        snprintf(buffer, BUFFER_SIZE, 
                 "HTTP/1.1 500 Internal Server Error\r\n"
                 "Content-Type: application/json\r\n"
                 "Content-Length: %d\r\n"
                 "Access-Control-Allow-Origin: *\r\n" // Allow any origin
                 "Connection: close\r\n\r\n%s", 
                 contentLength, response);
    }

    write(client_sock, buffer, strlen(buffer));
    shutdown(client_sock, SHUT_WR);
    read(client_sock, buffer, sizeof(buffer));
    close(client_sock);
}

int main() {
    int server_fd;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    setup_socket(&server_fd, &address);

    while (1) {
        printf("Waiting for HTTP request...\n");
        int new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen);
        if (new_socket < 0) {
            perror("Accept failed");
            continue;
        }

        respond_with_sensor_data(new_socket);
    }

    return 0;
}