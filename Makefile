PROJ=dht22-sensor-server
CC?=cc
CFLAGS=-Wall -Werror -O2

SRC_DIR=src
BIN_DIR=bin

SRC=$(wildcard $(SRC_DIR)/*.c)
OBJ=$(SRC:$(SRC_DIR)/%.c=$(BIN_DIR)/%.o)

all: $(BIN_DIR)/$(PROJ)

$(BIN_DIR)/$(PROJ): $(OBJ)
	$(CC) $(OBJ) -o $@

$(BIN_DIR)/%.o: $(SRC_DIR)/%.c | $(BIN_DIR)
	$(CC) $(CFLAGS) -c $< -o $@

# Create bin directory if it does not exist
$(BIN_DIR):
	mkdir -p $(BIN_DIR)

clean:
	rm -rf ./$(BIN_DIR)

.PHONY: all clean